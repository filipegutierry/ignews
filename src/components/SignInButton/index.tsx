import styles from "./styles.module.scss";
import { FaGithub } from "react-icons/fa";
import { FiX } from "react-icons/fi";

import { useState } from "react";

export function SignInButton() {
  const [isUserLoggedIn, setisUserLoggedIn] = useState(true);

  return isUserLoggedIn ? (
    <button className={styles.sigInButton} type="button">
      <FaGithub color="#04d301" />
      Filipe Gutierry
      <FiX className={styles.closeIcon}></FiX>
    </button>
  ) : (
    <button className={styles.sigInButton} type="button">
      <FaGithub color="#eba417" />
      Sign with Github
    </button>
  );
}
