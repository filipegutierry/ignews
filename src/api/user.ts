import { NextApiRequest, NextApiResponse } from "next";

export default (request: NextApiRequest, response: NextApiResponse) => {
  const users = [
    { id: 1, name: "Diego" },
    { id: 2, name: "Dani" },
    { id: 3, name: "Filipe" },
  ];

  return response.json(users);
};

// Utilizam a ideia de Serveless - Sobe um ambiente isolado e executa uma função e depois morre