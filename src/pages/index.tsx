import styles from "./home.module.scss";

import Head from "next/head";
import { SubscribeButton } from "../components/SubscribeButton";
import { stripe } from "../services/stripe";
import { GetStaticProps } from "next";

interface HomeProps {
  product: {
    priceId: string;
    amount: number;
  };
}

export default function Home({ product }: HomeProps) {
  return (
    <>
      <Head>
        <title>Ig.News</title>
      </Head>
      <main className={styles.contentContainer}>
        <section className={styles.hero}>
          <span>👏 Hey, welcome</span>
          <h1>
            News about the <span>React</span> world.
          </h1>
          <p>
            Get acces to all the publications <br />
            <span>for {product.amount} month</span>
            <SubscribeButton priceId={product.priceId} />
          </p>
        </section>
        <img src="/images/avatar.svg" alt="Girl coding"></img>
      </main>
    </>
  );
}

//getServerSideRender Ele é executado na camada do next/node e não do navegador

//getStaticProps -> é mais performatico e menos dinâmico. Sempre quando é preciso chamar as informações por usuário deve usar o serverSideRender
export const getStaticProps: GetStaticProps = async () => {
  const price = await stripe.prices.retrieve("price_1JCDSNLlR2eBE8fDfEXtzvWi", {
    expand: ["product"], //pra mostrar as informações do produto
  });

  const product = {
    priceId: price.id,
    amount: new Intl.NumberFormat("pt-BR", {
      style: "currency",
      currency: "BRL",
    }).format(price.unit_amount / 100),
  };

  return {
    props: {
      product,
    },
    revalidate: 60 * 60 * 24, //24 hours
  };
};

//########### NOTAS ##################################################
//Cada arquivo dentro de pages vira uma rota de página
//Instalar o TS ->  yarn add typescript @types/react @types/node -D e mudar os arquivos para .tsx
//Vamos utilizar o sass para fazer Css Scoped - Pra isso a gente coloca "modules" -> meuEstilo.modules.scss
//Vamos utilizar o sass

//File System Routes -> Sistema de roteamente via arquivos
//Toda vez que o usuário troca de tela, o _app.tsx é recriado, portanto ele fica recriando os states, etc

//SSG - Static Site Generation -> Ele salva um arquivo HTML estático (Isso é uma forma de cachear coisas que não mudam, tipo o preço da página home)

//Portanto no next tem 3 formas de fazer chamadas à apis
// 1 - Client-Side, usando um useEffect - O mais performático
// 2 - Server side Generation -  quando precisamos fazer dinamicamente chamadas a dados que precisa ser indexado ao google - chamadas dinâmicas + SEO
// 3 - Static Site Generation - Quando os dados não sofre muita alterações que queremos poupar processamento por parte do next - é uma forma de fazer cache + SEO

//### Vamos utilizar a autentcação do github

// API Routes